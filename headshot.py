#!/usr/bin/env python3

from ppadb.client import Client
from matplotlib import pyplot as plt
from csv import writer
import cv2
import time
import math
import joblib
import subprocess


adb = Client(host='127.0.0.1', port=5037)
devices = adb.devices()

if len(devices) == 0:
    print('no device attached')
    quit()

device = devices[0]

# postitions must have len = 1 or 2
def extract_human(positions):
    if (len(positions) < 2):
        x1, y1, w1, h1 = positions[0]
        # Base on what we see | 15px
        return (positions[0], (x1, y1 + 2 * h1 + 15, w1, h1))

    x1, y1, w1, h1 = positions[0]
    x2, y2, w2, h2 = positions[1]
    if y1 > y2:
        return (positions[1], positions[0])
    else:
        return (positions[0], positions[1])

def is_spinner_right_time(circle_positions):
    is_ok = False

    x1, y1 = circle_positions[0]
    x2, y2 = circle_positions[1]
    x3, y3 = circle_positions[2]

    if x2 == min(x1, x2, x3) and y2 < y1 and y2 > y3:
        is_ok = True

    return is_ok

def calc_has_shield(potentialPieces, boss_head, boss_leg):
    is_ok = False
    (hx, hy, _, hh) = boss_head
    (_, ly, _, lh) = boss_leg

    for i in range (len(potentialPieces)):
        (x, y, w, h) = potentialPieces[i]
        if x < hx and y > hy and y < ly + lh and (hx - x) < 3 * hh:
            is_ok = True

    return is_ok

is_able_to_play = False
num_of_tries = 0
num_fever_circles_occurred = 0
num_of_play = 1


train_data = []
positive_data = []

is_exception = False

while True:
    # Load the trained model
    model = joblib.load('train_data/trained_model.joblib')

    image = device.screencap()
    with open('screen.png', 'wb') as f:
        f.write(image)
    source = cv2.imread("screen.png")

    scr_height, scr_width, _ = source.shape
    min_x, min_y = scr_width, scr_height
    max_x = max_y = 0

    mask_human         = cv2.inRange(source, (50,0,0), (255, 50, 50))
    mask_waterfall     = cv2.inRange(source, (220,220,220), (251, 251, 251))
    mask_guard_spinner = cv2.inRange(source, (160, 251, 200), (255, 255, 255))
    mask_game_over     = cv2.inRange(source, (254, 254, 254), (255, 255, 255))
    mask_fever         = cv2.inRange(source, (0, 100, 240), (0, 200, 255))
    # mask_shield      = cv2.inRange(source, (100, 200, 105), (175, 248, 190)) # Still not good

    contour_human, hierarchy_human = cv2.findContours(mask_human.copy(),
                                                        cv2.RETR_EXTERNAL,
                                                        cv2.CHAIN_APPROX_SIMPLE)

    contour_waterfall, _ = cv2.findContours(mask_waterfall.copy(),
                                                        cv2.RETR_EXTERNAL,
                                                        cv2.CHAIN_APPROX_SIMPLE)

    contour_circle, _ = cv2.findContours(mask_guard_spinner.copy(),
                                                        cv2.RETR_EXTERNAL,
                                                        cv2.CHAIN_APPROX_SIMPLE)

    contour_game_over, _ = cv2.findContours(mask_game_over.copy(),
                                                        cv2.RETR_EXTERNAL,
                                                        cv2.CHAIN_APPROX_SIMPLE)

    contour_fever, _ = cv2.findContours(mask_fever.copy(),
                                                        cv2.RETR_EXTERNAL,
                                                        cv2.CHAIN_APPROX_SIMPLE)

    # contour_shield, _ = cv2.findContours(mask_shield.copy(),
    #                                                     cv2.RETR_EXTERNAL,
    #                                                     cv2.CHAIN_APPROX_SIMPLE)

    is_game_over = False
    for i in range (len(contour_game_over)):
        (x,y,w,h) = cv2.boundingRect(contour_game_over[i])
        if w > scr_width / 2 and h > scr_height / 4:
            # Has over popup
            is_game_over = True
            # cv2.rectangle(maskGameOver, (x,y), (x+w, y+h),(0,0,0), 10)

    if is_game_over:
        is_able_to_play = False
        num_of_play = num_of_play + 1 if num_of_tries > 0 else num_of_play
        if num_of_tries > 0:
            with open('train_data/positive.csv', 'a') as f_object:
                # Pass this file object to csv.writer()
                # and get a writer object
                writer_object = writer(f_object)
                # Pass the list as an argument into
                # the writerow()
                for i in range(len(positive_data)):
                    writer_object.writerow(positive_data[i])
                # Close the file object
                f_object.close()
                updatetrain = subprocess.Popen(f'python3 train.py', shell=True,
                                        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            train_data = []
            positive_data = []
        num_of_tries = 0
        # Auto re-play
        time.sleep(1)
        device.input_tap(scr_width / 2, 1700)
        # Prevent calculate immediately while boss is falling
        time.sleep(5)
    else:
        is_able_to_play = True

    if is_able_to_play == False:
        continue

    boss_positions = []
    player_positions = []
    for i in range (len(contour_human)):
        (x,y,w,h) = cv2.boundingRect(contour_human[i])

        if x > scr_width / 2 and w > 30 and h > 20:
            # Boss Right
            boss_positions.append((x, y, w, h))
            # cv2.rectangle(source, (x,y), (x+w,y+h), (255, 255, 255), 10)
        if x < scr_width / 2 and w > 30 and h > 20:
            # Player Left
            player_positions.append((x, y, w, h))
            # cv2.rectangle(source, (x,y), (x+w,y+h), (255, 255, 0), 10)

    boss_head, boss_leg = extract_human(boss_positions)
    player_head, player_leg = extract_human(player_positions)
    bhx, bhy, bhw, bhh = boss_head
    blx, bly, blw, blh = boss_leg
    plx, ply, plw, plh = player_leg
    player_x = plx + plw # Face
    player_y = ply + plh # Feet touch the ground
    boss_x = blx # Face
    boss_y = bhy + bhh # Neck

    fever_circles = []
    for i in range (len(contour_fever)):
        (_,_,w,h) = cv2.boundingRect(contour_fever[i])
        (x,y), radius = cv2.minEnclosingCircle(contour_fever[i])
        center = (int(x),int(y))
        radius = int(radius)
        if abs(w - h) < 5: # and radius < 20:
            # Have fever circles around player
            # Result will be ordered by Y (height)
            fever_circles.append(center)
            # cv2.circle(source, center,radius,(0,255,0), 10)

    guard_spinner_circles = []
    for i in range (len(contour_circle)):
        (_, _, w, h) = cv2.boundingRect(contour_circle[i])
        (x, y), radius = cv2.minEnclosingCircle(contour_circle[i])
        center = (int(x), int(y))
        radius = int(radius)
        if abs(w - h) < 5 and radius > 30 and radius < 60:
            # Have circles around boss
            # Result will be ordered by Y (height)
            guard_spinner_circles.append(center)
            # cv2.circle(source, center,radius,(0,255,0), 10)

    waterfall_lowest = 0
    waterfall_x = 0
    waterfall_w = 0
    for i in range (len(contour_waterfall)):
        (x,y,w,h) = cv2.boundingRect(contour_waterfall[i])
        if w > 100:
            # Has pieces of waterfall
            if x > waterfall_x:
                waterfall_x = x
            if y + h > waterfall_lowest:
                waterfall_lowest = y+h
            if waterfall_w == 0 or waterfall_w > w:
                waterfall_w = w

    calc_distance = boss_x - player_x
    calc_height = player_y - boss_y
    calc_angle_degree = int(math.degrees(math.atan( calc_height / calc_distance )))
    has_waterfall = waterfall_x > 0
    calc_waterfall_2_boss = boss_x - ((waterfall_x + waterfall_w) if has_waterfall else boss_x)
    has_guard_spinner = len(guard_spinner_circles) == 3
    has_fewer_circles = len(fever_circles) > 0
    has_shield = len(boss_positions) == 1 # Workaround

    if is_game_over == False and calc_height > 50 and calc_distance > scr_width / 2:
        calc_is_right_time = (has_guard_spinner and is_spinner_right_time(guard_spinner_circles))
        # If there's a guard spinner we should wait for the right time
        if has_guard_spinner == False or calc_is_right_time == True:
            num_of_tries = num_of_tries + 1
            if num_fever_circles_occurred == 2 and len(train_data):
                positive_data.append(train_data[-1])
            num_fever_circles_occurred = num_fever_circles_occurred + 1 if has_fewer_circles else 0
            has_fever = num_fever_circles_occurred == 2
            if num_fever_circles_occurred == 1 and len(train_data) > 1:
                positive_data.append(train_data[-2])
            if has_fever:
                positive_data.append(train_data[-1])
            if is_exception and len(train_data):
                positive_data.append(train_data[-1])
            # Default
            time_estimated = int(200 + (calc_angle_degree - 10) * (1000 - 200) / (90 - 10))
            # Run & observe to adjust
            time_estimated = (time_estimated + 40 if calc_angle_degree > 30 else
                            time_estimated + 25 if calc_angle_degree > 35 else
                            time_estimated + 30 if calc_angle_degree > 40 else
                            time_estimated + 25 if calc_angle_degree > 45 else
                            # time_estimated + 20 if calc_angle_degree > 50 else
                            time_estimated)

            time_estimated = time_estimated - calc_waterfall_2_boss if waterfall_x > 100 else time_estimated
            time_estimated = (time_estimated - 120 if has_fever and calc_angle_degree > 40 else
                              time_estimated - 90 if has_fever and calc_angle_degree > 30  else
                              time_estimated - 60 if has_fever else
                              time_estimated)

            ratio_hd = calc_height / calc_distance
            # Run & observe to adjust
            adjustment_time = int((ratio_hd - 1) * 200) if ratio_hd > 1 else -20
            time_estimated = max(185, time_estimated + adjustment_time)
            sleep_seconds = 0

            sleep_seconds = sleep_seconds + 0.1 if calc_is_right_time else sleep_seconds
            sleep_seconds = sleep_seconds + 0.1 if calc_is_right_time and calc_angle_degree > 40 else sleep_seconds
            sleep_seconds = sleep_seconds + 0.2 if calc_is_right_time and has_fever else sleep_seconds

            predictions = model.predict([(calc_distance, calc_height, calc_angle_degree, calc_waterfall_2_boss, has_guard_spinner, has_shield, has_fever)])
            (ml_estimated, ml_delay) = predictions.round().astype(int)[0]

            console_lines = ('---------------\n'
                f'{num_of_play}.{num_of_tries}. Player: {player_y}, Boss: {boss_y}\n'
                f'Distance: {calc_distance}, Height: {calc_height}, Angle: {calc_angle_degree}\n',
                f'Waterfall: {has_waterfall} ({calc_waterfall_2_boss}), Guard Spinner: {has_guard_spinner}, Shield: {has_shield}, Fever: {has_fever}\n'
                f'[FORMULA] Estimated: {time_estimated}, Delay: {sleep_seconds} - Adjustment: {adjustment_time}, Ratio: {ratio_hd}\n'
                f'[MACHINE] Estimated: {ml_estimated}, Delay: {ml_delay}'
                )
            train_data.append((calc_distance, calc_height, calc_angle_degree, calc_waterfall_2_boss, has_guard_spinner, has_shield, has_fever, time_estimated, sleep_seconds))
            print(''.join(console_lines))
            is_exception = has_waterfall or has_fever
            time.sleep(sleep_seconds)
            device.shell(f'input touchscreen swipe {scr_width / 2} {scr_height / 2} {scr_width / 2} {scr_height / 2} {time_estimated}')
            time.sleep(4)

    # plt.subplot(1, 1, 1)
    # plt.imshow(source)
    # plt.show()

    time.sleep(1) if is_game_over == True else time.sleep(0.3)