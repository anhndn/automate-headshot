import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error
import joblib

# Load the CSV data
data = pd.read_csv('train_data/positive.csv')

# Split the data into features (X) and target (y)
X = data[['d', 'h', 'a', 'w', 'g', 's', 'f']]
y = data[['e', 't']]

# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Initialize the Random Forest Regressor model
model = RandomForestRegressor(n_estimators=100, random_state=42)

# Train the model
model.fit(X_train, y_train)

# Save the trained model for future use
joblib.dump(model, 'train_data/trained_model.joblib')
